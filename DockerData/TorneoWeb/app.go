package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
)

//================================ URL DE MICROSERVICIOS ================================//
var torneoURL = ""

//============================ SERVICIOS DE LA PARTE LOGICA ============================//

type inicio struct {
	Email string
	Pass  string
}

func loguearse(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/login/"

	var decoder = json.NewDecoder(r.Body)
	var i inicio
	err := decoder.Decode(&i)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"email":"` + i.Email + `","password":"` + i.Pass + `"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	//fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

type torneo struct {
	Jid       string
	URL       string
	Jugadores []string
}

type partida struct {
	Tid string
}

func listaTorneo(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/listaTorneo/"

	req, err := http.NewRequest("GET", url, nil)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	//fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func selectTorneo(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/selectTorneo/"

	var decoder = json.NewDecoder(r.Body)
	var p partida
	err := decoder.Decode(&p)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"id":` + p.Tid + `}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	//fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func crearTorneo(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/crearTorneo/"

	var decoder = json.NewDecoder(r.Body)
	var t torneo
	err := decoder.Decode(&t)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"jid":` + t.Jid + `,"url":"` + t.URL + `","jugadores":[` + strings.Join(t.Jugadores, ",") + `]}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	//fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

type gensim struct {
	Pid       string
	Jugadores []string
	URL       string
	Tipo      int //0=generar 1=simular
}

func generarsimular(w http.ResponseWriter, r *http.Request) {
	var decoder = json.NewDecoder(r.Body)
	var gs gensim
	err := decoder.Decode(&gs)
	if err != nil {
		//panic(err)
	}

	// http://ip:port/
	var url = gs.URL

	if gs.Tipo == 0 {
		url = url + "generar/"
	} else {
		url = url + "simular/"
	}

	fmt.Println(url)

	var jsonStr = []byte(`{"id":"` + gs.Pid + `","jugadores":[` + strings.Join(gs.Jugadores, ",") + `]}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

type juego struct {
	Jid  string
	Name string
	URL  string
}

func listaJuego(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/listaJuego/"

	req, err := http.NewRequest("GET", url, nil)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	//fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func insertJuego(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/insertJuego/"

	var decoder = json.NewDecoder(r.Body)
	var j juego
	err := decoder.Decode(&j)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"nombre":"` + j.Name + `","link":"` + j.URL + `"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func updateJuego(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/updateJuego/"

	var decoder = json.NewDecoder(r.Body)
	var j juego
	err := decoder.Decode(&j)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"id":` + j.Jid + `,"nombre":"` + j.Name + `","link":"` + j.URL + `"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func deleteJuego(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/deleteJuego/"

	var decoder = json.NewDecoder(r.Body)
	var j juego
	err := decoder.Decode(&j)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"id":` + j.Jid + `}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

type usuario struct {
	UID      string
	Nombre   string
	Apellido string
	Email    string
	Pass     string
	Admin    bool
}

func listaUsuario(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/listaUsuario/"

	req, err := http.NewRequest("GET", url, nil)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	//fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func insertUsuario(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/insertUsuario/"

	var decoder = json.NewDecoder(r.Body)
	var u usuario
	err := decoder.Decode(&u)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"nombre":"` + u.Nombre + `","apellido":"` + u.Apellido + `","email":"` + u.Email + `","password":"` + u.Pass + `","admin":` + strconv.FormatBool(u.Admin) + `}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func updateUsuario(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/updateUsuario/"

	var decoder = json.NewDecoder(r.Body)
	var u usuario
	err := decoder.Decode(&u)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"id":` + u.UID + `,"nombre":"` + u.Nombre + `","apellido":"` + u.Apellido + `","email":"` + u.Email + `","password":"` + u.Pass + `","admin":` + strconv.FormatBool(u.Admin) + `}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func deleteUsuario(w http.ResponseWriter, r *http.Request) {
	var url = torneoURL + "/deleteUsuario/"

	var decoder = json.NewDecoder(r.Body)
	var u usuario
	err := decoder.Decode(&u)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"id":` + u.UID + `}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

//============================ SERVICIOS DE LA PARTE GRAFICA ============================//

func login(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("login.html"))
	t.Execute(w, "")
}

func juegos(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("juegos.html"))
	t.Execute(w, "")
}

func usuarios(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("usuarios.html"))
	t.Execute(w, "")
}

func index(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("index.html"))
	t.Execute(w, "")
}

func main() {
	//================== URL JUEGO ==================//
	torneoip, deftip := os.LookupEnv("TORNEOIP")
	torneoport, deftport := os.LookupEnv("TORNEOPORT")

	if !deftip {
		torneoip = "torneo"
	}

	if !deftport {
		torneoport = "8000"
	}

	torneoURL = "http://" + torneoip + ":" + torneoport

	//================== URL PROPIA ==================//
	ip, defip := os.LookupEnv("GOIP")
	port, defport := os.LookupEnv("GOPORT")

	if !defip {
		ip = "tinterfaz"
	}

	if !defport {
		port = "11000"
	}

	//archivos estaticos
	http.Handle("/logic/", http.StripPrefix("/logic/", http.FileServer(http.Dir("logic/"))))
	http.Handle("/style/", http.StripPrefix("/style/", http.FileServer(http.Dir("style/"))))

	//endpoint para interfaz
	http.HandleFunc("/", login)
	http.HandleFunc("/index", index)
	http.HandleFunc("/usuarios", usuarios)
	http.HandleFunc("/juegos", juegos)

	//endpoint para peticiones
	http.HandleFunc("/loguearse", loguearse)

	http.HandleFunc("/listaTorneo", listaTorneo)
	http.HandleFunc("/selectTorneo", selectTorneo)
	http.HandleFunc("/crearTorneo", crearTorneo)
	http.HandleFunc("/generarsimular", generarsimular)

	http.HandleFunc("/listaJuego", listaJuego)
	http.HandleFunc("/insertJuego", insertJuego)
	http.HandleFunc("/updateJuego", updateJuego)
	http.HandleFunc("/deleteJuego", deleteJuego)

	http.HandleFunc("/listaUsuario", listaUsuario)
	http.HandleFunc("/insertUsuario", insertUsuario)
	http.HandleFunc("/updateUsuario", updateUsuario)
	http.HandleFunc("/deleteUsuario", deleteUsuario)

	//servidor
	fmt.Println("Escuchando por IP:" + ip + " PORT:" + port)
	http.ListenAndServe(":"+port, nil)
}
