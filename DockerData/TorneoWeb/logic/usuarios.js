
function initUser(){
    var logeado = sessionStorage.getItem('logueado');
    if(!logeado){
        window.location.href = "../";
    }

    //mostrar el nombre del usuario
    var nombre = sessionStorage.getItem('nombre-usuario');
    document.getElementById("nombre_user").innerText = nombre;

    //limpiamos los campos
    limpiar();

    //cargamos los dropdown
    fetch('../listaUsuario',{
        method: 'GET'
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => viewUser(response));
}

function regresar(){
    window.location.href = "../index";
}


//==================================== SELECT ====================================//

var array_users_info = [];

function viewUser(response){
    var array = JSON.parse(JSON.stringify(response));

    //guardamos la info de los usuarios
    array_users_info = array;

    //lista de usuarios para modificar
    var upd = document.getElementById("users_m");

    //lista de usuarios para eliminar
    var del = document.getElementById("users_e");

    array.forEach(element => {
        var btnu = document.createElement("button");
        btnu.setAttribute('id',element.id);
        btnu.setAttribute('onclick','selectUser(this);');
        btnu.setAttribute("class","dropdown-item");
        btnu.innerText = element.nombres;

        upd.appendChild(btnu);

        var btnd = document.createElement("button");
        btnd.setAttribute('id',element.id);
        btnd.setAttribute('onclick','deleteUser(this);');
        btnd.setAttribute("class","dropdown-item");
        btnd.innerText = element.nombres;

        del.appendChild(btnd);
    });
}


//===================================== CREAR =====================================//

function addUser(){
    var email = document.getElementById("a-email").value;
    var nombre = document.getElementById("a-nombre").value;
    var apellido = document.getElementById("a-apellido").value;
    var pass = document.getElementById("a-pass").value;
    var admin = document.getElementById("a-admin").checked;

    if(nombre != "" && apellido != "" && pass != "" && email != ""){
        fetch('../insertUsuario',{
            method: 'POST',
            body: JSON.stringify({
                "UID":0,
                "Nombre":nombre,
                "Apellido":apellido,
                "Email":email,
                "Pass":pass,
                "Admin":admin
            }),
            headers:{
              'Content-Type': 'application/json'
            }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => location.reload());
    }
}


//=================================== MODIFICAR ===================================//

function selectUser(btn){
    var index = array_users_info.findIndex(
        function(element){ 
            return element.id == btn.id;
        }
    );

    //encontramos el usuario pedido
    var user = array_users_info[index];

    document.getElementById("m-id").textContent = btn.id;
    document.getElementById("m-email").value = user.email;
    document.getElementById("m-nombre").value = user.nombres;
    document.getElementById("m-apellido").value = user.apellidos;
    document.getElementById("m-pass").value = user.password;
    document.getElementById("m-admin").checked = user.administrador;
}

function updateUser(){
    var id = document.getElementById("m-id").textContent;
    var email = document.getElementById("m-email").value;
    var nombre = document.getElementById("m-nombre").value;
    var apellido = document.getElementById("m-apellido").value;
    var pass = document.getElementById("m-pass").value;
    var admin = document.getElementById("m-admin").checked;

    fetch('../updateUsuario',{
        method: 'POST',
        body: JSON.stringify({
            "UID":id,
            "Nombre":nombre,
            "Apellido":apellido,
            "Email":email,
            "Pass":pass,
            "Admin":admin
        }),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => location.reload());
}


//==================================== ELIMINAR ====================================//

function deleteUser(btn){
    var id = btn.id;

    fetch('../deleteUsuario',{
        method: 'POST',
        body: JSON.stringify({
            "UID":id,
            "Nombre":"",
            "Apellido":"",
            "Email":"",
            "Pass":"",
            "Admin":""
        }),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => location.reload());
}


//==================================== LIMPIAR ====================================//

function limpiar(){
    document.getElementById("a-email").value = '';
    document.getElementById("a-nombre").value = '';
    document.getElementById("a-apellido").value = '';
    document.getElementById("a-pass").value = '';
    document.getElementById("a-admin").checked = false;

    document.getElementById("m-id").style.visibility = "hidden";
    document.getElementById("m-id").textContent = '';

    document.getElementById("m-email").value = '';
    document.getElementById("m-nombre").value = '';
    document.getElementById("m-apellido").value = '';
    document.getElementById("m-pass").value = '';
    document.getElementById("m-admin").checked = false;
}