
CREATE TABLE partida(
  partida VARCHAR(100) NOT NULL,
  jugador1 BIGINT NOT NULL,
  jugador2 BIGINT NOT NULL,
  score1 INTEGER NOT NULL DEFAULT 0,
  score2 INTEGER NOT NULL DEFAULT 0,

  PRIMARY KEY (partida, jugador1, jugador2)
);