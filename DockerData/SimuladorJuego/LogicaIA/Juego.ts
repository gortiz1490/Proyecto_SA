import { MatrizJuego } from "./MatrizJuego";

class Juego {

    private mJugador:MatrizJuego;   //Matriz de juego
    private nJugador:String;        //Nombre

    constructor(nJugador:String){
        this.mJugador = new MatrizJuego();
        this.nJugador = nJugador;
    }

    decision(dados:Array<Number>, numtiro:Number, pts:Number, pM:Number){
        //rep[UNOS, DOS, TRES, CUATROS, CINCOS, SEISES]
        var rep:Array<Number> = [0,0,0,0,0,0];

        dados.forEach(element => {
            rep[Number(element) - 1] = Number(rep[Number(element) - 1]) + 1;
        });

        //Pedimos los puntos actuales
        var puntos:Number = pts;
        //Pedimos la pos de juego actual
        var posMatriz:Number = pM;

        /*=============================================================================*/

        if( (rep.join("").includes("5")) && (this.mJugador.getPunteo(14) == -1) ){
            /**
             * Una de las caras de los dados salio 5 veces
             * y verificamos que la casilla de la matriz 
             * aun este libre.
             */
            if(puntos < 50){
                puntos = 50;    //Puntos que podemos obtener
                posMatriz = 14; //Posicion del tablero escogida
            }
        }
        
        /*=============================================================================*/

        if( (rep[0]>=1 && rep[1]>=1 && rep[2]>=1 && rep[3]>=1 && rep[4]>=1) &&
            (this.mJugador.getPunteo(12) == -1) ){
            /**
             * Tenemos una escalera grande con 1,2,3,4,5
             * y verificamos que la posicion este libre 
             * en el tablero
             */
            if(puntos < 40){
                puntos = 40;    //Puntos que podemos obtener
                posMatriz = 12; //Posicion del tablero escogida
            }
        }

        if( (rep[1]>=1 && rep[2]>=1 && rep[3]>=1 && rep[4]>=1 && rep[5]>=1) && 
            (this.mJugador.getPunteo(12) == -1) ){
            /**
             * Tenemos una escalera grande con 2,3,4,5,6
             * y verificamos que la posicion este libre 
             * en el tablero
             */
            if(puntos < 40){
                puntos = 40;    //Puntos que podemos obtener
                posMatriz = 12; //Posicion del tablero escogida
            }
        }
        
        /*=============================================================================*/

        if( (rep[0]>=1 && rep[1]>=1 && rep[2]>=1 && rep[3]>=1) &&
            (this.mJugador.getPunteo(11) == -1) ){
            /**
             * Tenemos una escalera pequena con 1,2,3,4
             * y verificamos que la posicion este libre 
             * en el tablero
             */
            if(puntos < 30){
                puntos = 30;    //Puntos que podemos obtener
                posMatriz = 11; //Posicion del tablero escogida
            }
        }
        
        if( (rep[1]>=1 && rep[2]>=1 && rep[3]>=1 && rep[4]>=1) &&
            (this.mJugador.getPunteo(11) == -1) ){
            /**
             * Tenemos una escalera pequena con 2,3,4,5
             * y verificamos que la posicion este libre 
             * en el tablero
             */
            if(puntos < 30){
                puntos = 30;    //Puntos que podemos obtener
                posMatriz = 11; //Posicion del tablero escogida
            }
        }
        
        if( (rep[2]>=1 && rep[3]>=1 && rep[4]>=1 && rep[5]>=1) &&
            (this.mJugador.getPunteo(11) == -1) ){
            /**
             * Tenemos una escalera pequena con 3,4,5,6
             * y verificamos que la posicion este libre 
             * en el tablero
             */
            if(puntos < 30){
                puntos = 30;    //Puntos que podemos obtener
                posMatriz = 11; //Posicion del tablero escogida
            }
        }
        
        /*=============================================================================*/

        if( (rep.join("").includes("3") && rep.join("").includes("2")) &&
            (this.mJugador.getPunteo(10) == -1) ){
            /**
             * Tenemos un full house con un trio y un par
             * y verificamos que la posicion este libre 
             * en el tablero
             */
            if(puntos < 25){
                puntos = 25;    //Puntos que podemos obtener
                posMatriz = 10; //Posicion del tablero escogida
            }
        }

        /*=============================================================================*/
        
        if( (rep.join("").includes("4") || rep.join("").includes("5")) &&
            (this.mJugador.getPunteo(9) == -1) ){
            /**
             * Tenemos cuatro dados iguales
             * y verificamos que la posicion 
             * este libre en el tablero
             */
            var temp:number = 0;
            for(var i = 0; i < rep.length; i++){
                temp = temp + Number(rep[i]) * (i + 1);
            }

            if(puntos < temp){
                puntos = temp;  //Puntos que podemos obtener
                posMatriz = 9;  //Posicion del tablero escogida
            }
        }

        /*=============================================================================*/
        
        if( (rep.join("").includes("3") || rep.join("").includes("4") || rep.join("").includes("5")) &&
            (this.mJugador.getPunteo(8) == -1) ){
            /**
             * Tenemos tres dados iguales
             * y verificamos que la posicion 
             * este libre en el tablero
             */
            var temp:number = 0;
            for(var i = 0; i < rep.length; i++){
                temp = temp + Number(rep[i]) * (i + 1);
            }

            if(puntos < temp){
                puntos = temp;  //Puntos que podemos obtener
                posMatriz = 8;  //Posicion del tablero escogida
            }
        }

        /*=============================================================================*/

        //Aun no es el ultimo tiro podemos
        //intentar una mejor jugada
        if(numtiro < 3){
            return [puntos, posMatriz]
        }

        /**
         * No se logro ninguna jugada anterior por lo cual 
         * podemos utilizar la suma de los dados de alguna 
         * de las caras para verificar si podemos obtener 
         * un punteo.
         */

        if(this.mJugador.getPunteo(5) == -1){
            /**
             * Verificamos que la posicion
             * de la suma de 6 este libre 
             * en el tablero
             */
            var temp:number = Number(rep[5]) * 6;

            if(puntos < temp){
                puntos = temp;  //Puntos que podemos obtener
                posMatriz = 5;  //Posicion del tablero escogida
            }
        }


        if(this.mJugador.getPunteo(4) == -1){
            /**
             * Verificamos que la posicion
             * de la suma de 5 este libre 
             * en el tablero
             */
            var temp:number = Number(rep[4]) * 5;

            if(puntos < temp){
                puntos = temp;  //Puntos que podemos obtener
                posMatriz = 4;  //Posicion del tablero escogida
            }
        }


        if(this.mJugador.getPunteo(3) == -1){
            /**
             * Verificamos que la posicion
             * de la suma de 4 este libre 
             * en el tablero
             */
            var temp:number = Number(rep[3]) * 4;

            if(puntos < temp){
                puntos = temp;  //Puntos que podemos obtener
                posMatriz = 3;  //Posicion del tablero escogida
            }
        }


        if(this.mJugador.getPunteo(2) == -1){
            /**
             * Verificamos que la posicion
             * de la suma de 3 este libre 
             * en el tablero
             */
            var temp:number = Number(rep[2]) * 3;

            if(puntos < temp){
                puntos = temp;  //Puntos que podemos obtener
                posMatriz = 2;  //Posicion del tablero escogida
            }
        }


        if(this.mJugador.getPunteo(1) == -1){
            /**
             * Verificamos que la posicion
             * de la suma de 2 este libre 
             * en el tablero
             */
            var temp:number = Number(rep[1]) * 2;

            if(puntos < temp){
                puntos = temp;  //Puntos que podemos obtener
                posMatriz = 1;  //Posicion del tablero escogida
            }
        }


        if(this.mJugador.getPunteo(0) == -1){
            /**
             * Verificamos que la posicion
             * de la suma de 1 este libre 
             * en el tablero
             */
            var temp:number = Number(rep[0]) * 1;

            if(puntos < temp){
                puntos = temp;  //Puntos que podemos obtener
                posMatriz = 0;  //Posicion del tablero escogida
            }
        }

        /*=============================================================================*/

        /**
         * Si no tenemos otra opcion entonces 
         * podemos marcar el chance como ultima
         * alternativa de juego
         */

        if( (posMatriz) == -1 && (this.mJugador.getPunteo(13) == -1) ){
            /**
             * No entro a las opciones
             * anteriores de juego, por
             * lo cual marcamos chance
             */
            var temp:number = 0;
            for(var i = 0; i < rep.length; i++){
                temp = temp + Number(rep[i]) * (i + 1);
            }

            if(puntos < temp){
                puntos = temp;  //Puntos que podemos obtener
                posMatriz = 13; //Posicion del tablero escogida
            }
        }

        //Terminamos el total de tiros
        return [puntos, posMatriz]
    }

    tomarTurno(){
        //Varificamos que no termino
        //su tablero de juego
        if(this.juegoTerminado()){
            return "Jugador: " + this.nJugador + " Tablero: [" + this.mJugador.toString() + "]";
        }

        var dados:Array<Number> = [];
        var numDados = 5;

        //juagada [puntos, posMatriz]
        var jugada:Array<Number> = [0, -1];


        //Tenemos un total de 3 tiros
        for(var tiro = 1; tiro < 4; tiro++){

            //Enviamos una peticion de tiro de dados
            //Obtenemos el arreglos y lo almacenamos
            //en la variable dados

            for(var t = 0; t <= numDados; t ++){
                dados[t] = Math.floor(Math.random() * (7 - 1)) + 1;
            }

            //LA RESTRICCION ES QUE SIEMPRE SE JUGARA 
            //CON LOS 5 DADOS PARA SIMPLIFICAR LA
            //LOGICA DEL JUEGO.

            //Enviamos los dados para la decision del tiro
            jugada = this.decision(dados, tiro, jugada[0], jugada[1]);
        }


        //Luego de realizar los tiros debemos actualizar el tablero
        if(jugada[1] != -1){
            //validamos la jugada
            this.mJugador.setPunteo(jugada[1], jugada[0]);
        }

        return "Jugador: " + this.nJugador + " Tablero: [" + this.mJugador.toString() + "]";
    }

    juegoTerminado():boolean{
        return this.mJugador.llena();
    }

    puntajeFinal():Number {
        return this.mJugador.punteoFinal();
    }
    
}
export{Juego};