var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var fs = require('fs');
var axios = require('axios');
var mysql = require('mysql');
var path = require('path');
var crypto = require("crypto");
var auth = require(path.resolve(__dirname, './authToken.js'));

import { AxiosError, AxiosResponse } from "axios";
import {Response, Request} from "express";
import { MysqlError } from "mysql";

const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));


/*Variables de entorno donde correra la API Torneo*/
const ip = process.env.IP || "torneo";
const port = process.env.PORT || 8000;

/*Variables de entorno donde correra la API Usuario*/
const usersip = process.env.USERIP || "user";
const usersport = process.env.USERPORT || 9000;

/*Variables de entorno para la conexion con la BD*/
const DBHOST = process.env.DBHOST || "torneodb";
const USER = process.env.DBUSERNAME || "root";
const PASS = process.env.DBPASSWORD || "123456789";
const DBNAME = process.env.DBNAME || "torneoDB";

//========================================= BD MYSQL =========================================//

/*Conexion con la BD MYSQL*/
var conn = mysql.createConnection({
    host: DBHOST,
    user: USER,
    password: PASS,
    database: DBNAME
});


//========================================== TORNEO ==========================================//

app.get("/", (req: Request, res: Response) => {
    res.send("Api torneo lista");
});


app.get("/listaTorneo/", async (req: Request, res: Response) => {
    var sql = "SELECT * FROM torneo;";

    conn.query(sql, function (err:MysqlError, result:Object[]) {
        if(err) res.status(406).send(JSON.stringify([err]));
        res.status(201).send(JSON.stringify(result));
    });
});


app.post("/selectTorneo/", async (req: Request, res: Response) => {
    var id:Number = req.body.id; // id del torneo

    var sql = "SELECT * FROM llave,partido,torneo WHERE partido.id_partido=llave.id_partido AND partido.id_torneo=torneo.id_torneo AND torneo.id_torneo="+id+";";

    conn.query(sql, function (err:MysqlError, result:Object[]) {
        if(err) res.status(406).send(JSON.stringify([err]));
        res.status(201).send(JSON.stringify(result));
    });
});


app.post("/crearTorneo/", async (req: Request, res: Response) => {
    var idJuego:number = req.body.jid; // id del juego para el torneo
    var urlJuego:String = req.body.url; // url del juego para el torneo
    var jugadores:Array<number> = req.body.jugadores; //array de jugadores
    var cantJug:Number = jugadores.length; //cantidad de jugadores

    //verificamos la llave
    var par:Number = jugadores.length / 2; //cantidad de partidas iniciales
    var llave_valida:boolean = false;

    for(var i = 0;i <= 10;i++){
        //validamos si es llave de 0 a 10
        if(Math.pow(2,i) == par){
            llave_valida = true;
            break;
        }
    }

    if(Number(cantJug) % 2 == 0 && llave_valida){
        var partidas = Number(cantJug) / 2; // cantidad de partidas

        //crearTorneo
        var idTorneo:any = await crearTorneo(idJuego, urlJuego, partidas);

        
        //una matriz de partidas por jornada
        var jornadas:Array<Array<String>> = new Array<Array<String>>();

        //random array jugadores
        var cont1:number = -2;
        var cont2:number = -1;
        jugadores.sort(function(a,b) {return (Math.random()-0.5)});


        //========================= PRIMERA JORNADA =========================//
        var idpartidas:Array<String> = new Array<String>();

        for(let index = 0; index < partidas; index++){
            cont1 = cont1 + 2;
            cont2 = cont2 + 2;
            const p1:Number = jugadores[cont1];
            const p2:Number = jugadores[cont2];

            //generamos un nuevo uuid de la partida
            var newuuid:String = crypto.randomBytes(16).toString("hex");

            var idPartido:any = await crearPartidas(idTorneo, newuuid, p1, p2);
            idpartidas.push(newuuid);
        }

        jornadas.push(idpartidas);


        //========================= PROXIMAS JORNADAS =========================//
        partidas = partidas / 2;
        while (partidas >= 1) {
            //creamos las siguientes jornadas
            idpartidas = new Array<String>();

            for(let index = 0; index < partidas; index++){
                //generamos un nuevo uuid de la partida
                var newuuid:String = crypto.randomBytes(16).toString("hex");
    
                var idPartido:any = await crearPartidas(idTorneo, newuuid, 0, 0);
                idpartidas.push(newuuid);
            }

            jornadas.push(idpartidas);

            partidas = partidas / 2;
        }


        //========================= FLUJO DEL TORNEO =========================//
        for(var i = 1; i < jornadas.length; i++){
            var pos = 0;
            for(var j = 0; j<jornadas[i].length; j++){
                crearflujo(jornadas[i][j], jornadas[i-1][pos],jornadas[i-1][pos+1]);
                pos += 2;
            }
        }

        fs.appendFile('./Log/bitacora.txt',"Torneo Creado:"+idTorneo+" Partidas:"+jornadas.length+".\n", function(){});

        res.send(JSON.stringify({201:"Torneo Creado","TorneoId":idTorneo,"Partidas:":jornadas}));
    }else{
        res.send(JSON.stringify({406: "Número de jugadores inválido"}));
    }
});


function crearTorneo(idJuego:number, urlJuego:String, partidas:number){
    return new Promise((resolve) => {
        var sql = "INSERT INTO torneo (id_juego, url_juego, n_partidas) VALUES ("+idJuego+",'"+urlJuego+"',"+partidas+");";
        conn.query(sql, (err: any, res: { insertId: any; }) => {
            if (err) return;
            resolve(res.insertId);
        });
    });
}


function crearPartidas(idTorneo:number, idPartida:String, idJ1:Number, idJ2:Number){
    var sql = "INSERT INTO partido (id_partido,id_torneo) VALUES ('"+idPartida+"',"+idTorneo+");";

    conn.query(sql, (err:MysqlError, result:Object[]) => {
        if(err) return;

        var sqlp = "INSERT INTO llave (id_partido,id_jugador1,id_jugador2) VALUES ('"+idPartida+"',"+idJ1+","+idJ2+");";
        conn.query(sqlp, (err:MysqlError, result:Object[]) => {
            if(err) return;
            return;
        });
    });
}


function crearflujo(idAct:String, idPre1:String, idPre2:String){
    var sql = "INSERT INTO flujoPartida (idAct,idPre1,idPre2) VALUES ('"+idAct+"','"+idPre1+"','"+idPre2+"');";
    conn.query(sql, (err:MysqlError, result:Object[]) => {
        if(err) return;
        return;
    });
}


function setMarcador(idPartida:String, mp1:Number, mp2:Number){
    var sql = "UPDATE llave SET score_jugador1="+mp1+", score_jugador2="+mp2+" WHERE id_partido='"+idPartida+"';";

    conn.query(sql, (err:MysqlError, result:Object[]) => {
        if (err) console.log(err);
        console.log("Marcador de partida actualizado: "+idPartida);
    });
}


function ganador(idPartida:String, ganador:String){
    //ganador = [1, 2]
    var sql = "SELECT id_jugador"+ganador+" AS jugador FROM llave WHERE id_partido='"+idPartida+"';";

    conn.query(sql, (err:MysqlError, result:any) => {
        if (err) console.log(err);

        if(result.length > 0){
            var winner:Number = result[0].jugador; //id del ganador
            partidaSiguiente(idPartida, winner);
        }else{
            console.log("No se encontro la partida");
        }
    });
}


function partidaSiguiente(idPartida:String, jugador:Number) {
    //vemos si toca local
    var sqll = "SELECT idAct FROM flujoPartida WHERE idPre1='"+idPartida+"';";
    conn.query(sqll, (err:MysqlError, result:any) => {
        if (err) console.log(err);

        if(result.length > 0){
            //actualizamos esa partida
            actPartida(result[0].idAct, jugador, true);
        }else{
            //vemos si es visita
            var sqlv = "SELECT idAct FROM flujoPartida WHERE idPre2='"+idPartida+"';";
            conn.query(sqlv, (err:MysqlError, result:any) => {
                if (err) console.log(err);
    
                if(result.length > 0){
                    //actualizamos esa partida
                    actPartida(result[0].idAct, jugador, false);
                }
            });
        }
    });
}


function actPartida(idPartida:String, idJugador:Number, tipo:boolean){
    // tipo=true local, tipo=false visita

    var p:String = "id_jugador2"; //visita
    if(tipo){
        p = "id_jugador1"; //local
    }

    var sql = "UPDATE llave SET "+p+"="+idJugador+" WHERE id_partido='"+idPartida+"';";
    conn.query(sql, (err:MysqlError, result:Object[]) => {
        if (err) console.log(err);
        fs.appendFile('./Log/bitacora.txt',"Actualizacion en flujo de partidas.\n", function(){});
        console.log("Se actualizo el flujo de las partidas");
    });
}


app.put('/partidas/:id', (req: Request, res: Response) => {
    var partida:String = req.params.id;// id de la partida
    var m:Array<Number> = req.body.marcador;// marcador

    if(m[0] > m[1]){
        fs.appendFile('./Log/bitacora.txt',"Partida:"+partida+" Ganador: Local.\n", function(){});
        console.log("Ganador Jugador 1");
        //setear el marcador
        setMarcador(partida, m[0], m[1]);
        ganador(partida,"1");
    }else if(m[1] > m[0]){
        fs.appendFile('./Log/bitacora.txt',"Partida:"+partida+" Ganador: Visita.\n", function(){});
        console.log("Ganador Jugador 2");
        //setear el marcador
        setMarcador(partida, m[0], m[1]);
        ganador(partida,"2");
    }

    res.send(JSON.stringify({201:"Ganador asignado","partida":partida}));
});


//====================================== JUEGO ======================================//

app.get("/listaJuego", (req: Request, res: Response) => {
    var sql = "SELECT * FROM juego;";

    conn.query(sql, function (err:MysqlError, result:Object[]) {
        if(err) res.status(406).send(JSON.stringify([err]));
        res.status(201).send(JSON.stringify(result));
    });
});


app.post("/insertJuego", (req: Request, res: Response) => {
    var sql = "INSERT INTO juego(link,nombre) VALUES('" + req.body.link + "','" + req.body.nombre + "');";

    conn.query(sql, function (err:MysqlError, result:Object[]) {
        if(err) res.status(406).send(JSON.stringify({406: "Error en realizar insert de juegos"}));
        res.status(201).send(JSON.stringify({201:"Juego insertado"}));
    });
});


app.post("/updateJuego", (req: Request, res: Response) => {
    var sql = "UPDATE juego SET link='"+req.body.link+"', nombre='"+req.body.nombre+"' WHERE id_juego="+req.body.id+";";

    conn.query(sql, function (err:MysqlError, result:Object[]) {
        if(err) res.status(406).send(JSON.stringify({406: "Error en realizar update de juegos"}));
        res.send(JSON.stringify({201:"Juego actualizado"}));
    });
});


app.post("/deleteJuego", (req: Request, res: Response) => {
    var sql = "DELETE FROM juego WHERE id_juego="+req.body.id+";";

    conn.query(sql, function (err:MysqlError, result:Object[]) {
        if(err) res.status(406).send(JSON.stringify({406: "Error en realizar delete de juegos"}));
        res.status(201).send(JSON.stringify({201:"Juego eliminado"}));
    });
});


//===================================== USUARIO =====================================//

app.get("/listaUsuario/", auth.authtoken, async (req: Request, res: Response) => {
    axios.get('http://'+usersip+':'+usersport+'/jugadores/jugadores/')
    .then(function (response:AxiosResponse) {
        res.send(JSON.stringify(response.data));
    }).catch(function (error:AxiosError) {
        res.send(error.message);
    });
});


app.post("/insertUsuario/", auth.authtoken, async (req: Request, res: Response) => {
    axios.post('http://'+usersip+':'+usersport+'/jugadores/', {
        email: req.body.email,
        password: req.body.password,
        nombres: req.body.nombre,
        apellidos: req.body.apellido,
        administrador: req.body.admin
    }).then(function (response:AxiosResponse) {
        res.send(JSON.stringify(response.data));
    }).catch(function (error:AxiosError) {
        res.send(error.message);
    });
});


app.post("/updateUsuario/", auth.authtoken, async (req: Request, res: Response) => {
    axios.put('http://'+usersip+':'+usersport+'/jugadores/'+req.body.id, {
        email: req.body.email,
        password: req.body.password,
        nombres: req.body.nombre,
        apellidos: req.body.apellido,
        administrador: req.body.admin
    }).then(function (response:AxiosResponse) {
        res.send(JSON.stringify(response.data));
    }).catch(function (error:AxiosError) {
        res.send(error.message);
    });
});


app.post("/deleteUsuario/", auth.authtoken, async (req: Request, res: Response) => {
    //No hay un endpoint definido en la api para usuarios
    res.status(201).send("No se puede eliminar usuarios");
});


app.post("/login/", auth.authtoken, async (req: Request, res: Response) => {
    var email = req.body.email;
    var pass = req.body.password;

    axios.get('http://'+usersip+':'+usersport+'/login/?email='+email+'&password='+pass)
    .then(function (response:AxiosResponse) {
        res.send(JSON.stringify(response.data));
    }).catch(function (error:AxiosError) {
        res.send(error.message);
    });
});


//=================================================================================//

app.listen(port, ip, async () => {
    console.log('Torneo se escucha en el puerto: %d y con la ip: %s', port, ip);
});