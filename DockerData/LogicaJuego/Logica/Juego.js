"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Juego = void 0;
const MatrizJuego_1 = require("./MatrizJuego");
class Juego {
    constructor(nJugador) {
        this.mJugador = new MatrizJuego_1.MatrizJuego();
        this.nJugador = nJugador;
    }
    decision(dados) {
        //rep[UNOS, DOS, TRES, CUATROS, CINCOS, SEISES]
        var rep = [0, 0, 0, 0, 0, 0];
        dados.forEach(element => {
            rep[Number(element) - 1] = Number(rep[Number(element) - 1]) + 1;
        });
        //Posibles juegos que podemos tener
        var posibilidades = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
        /*=============================================================================*/
        if ((rep.join("").includes("5")) && (this.mJugador.getPunteo(14) == -1)) {
            /**
             * Una de las caras de los dados salio 5 veces
             * y verificamos que la casilla de la matriz
             * aun este libre.
             */
            posibilidades[14] = 50;
        }
        /*=============================================================================*/
        if ((rep[0] >= 1 && rep[1] >= 1 && rep[2] >= 1 && rep[3] >= 1 && rep[4] >= 1) &&
            (this.mJugador.getPunteo(12) == -1)) {
            /**
             * Tenemos una escalera grande con 1,2,3,4,5
             * y verificamos que la posicion este libre
             * en el tablero
             */
            posibilidades[12] = 40;
        }
        if ((rep[1] >= 1 && rep[2] >= 1 && rep[3] >= 1 && rep[4] >= 1 && rep[5] >= 1) &&
            (this.mJugador.getPunteo(12) == -1)) {
            /**
             * Tenemos una escalera grande con 2,3,4,5,6
             * y verificamos que la posicion este libre
             * en el tablero
             */
            posibilidades[12] = 40;
        }
        /*=============================================================================*/
        if ((rep[0] >= 1 && rep[1] >= 1 && rep[2] >= 1 && rep[3] >= 1) &&
            (this.mJugador.getPunteo(11) == -1)) {
            /**
             * Tenemos una escalera pequena con 1,2,3,4
             * y verificamos que la posicion este libre
             * en el tablero
             */
            posibilidades[11] = 30;
        }
        if ((rep[1] >= 1 && rep[2] >= 1 && rep[3] >= 1 && rep[4] >= 1) &&
            (this.mJugador.getPunteo(11) == -1)) {
            /**
             * Tenemos una escalera pequena con 2,3,4,5
             * y verificamos que la posicion este libre
             * en el tablero
             */
            posibilidades[11] = 30;
        }
        if ((rep[2] >= 1 && rep[3] >= 1 && rep[4] >= 1 && rep[5] >= 1) &&
            (this.mJugador.getPunteo(11) == -1)) {
            /**
             * Tenemos una escalera pequena con 3,4,5,6
             * y verificamos que la posicion este libre
             * en el tablero
             */
            posibilidades[11] = 30;
        }
        /*=============================================================================*/
        if ((rep.join("").includes("3") && rep.join("").includes("2")) &&
            (this.mJugador.getPunteo(10) == -1)) {
            /**
             * Tenemos un full house con un trio y un par
             * y verificamos que la posicion este libre
             * en el tablero
             */
            posibilidades[10] = 25;
        }
        /*=============================================================================*/
        if ((rep.join("").includes("4") || rep.join("").includes("5")) &&
            (this.mJugador.getPunteo(9) == -1)) {
            /**
             * Tenemos cuatro dados iguales
             * y verificamos que la posicion
             * este libre en el tablero
             */
            var temp = 0;
            for (var i = 0; i < rep.length; i++) {
                temp = temp + Number(rep[i]) * (i + 1);
            }
            posibilidades[9] = temp;
        }
        /*=============================================================================*/
        if ((rep.join("").includes("3") || rep.join("").includes("4") || rep.join("").includes("5")) &&
            (this.mJugador.getPunteo(8) == -1)) {
            /**
             * Tenemos tres dados iguales
             * y verificamos que la posicion
             * este libre en el tablero
             */
            var temp = 0;
            for (var i = 0; i < rep.length; i++) {
                temp = temp + Number(rep[i]) * (i + 1);
            }
            posibilidades[8] = temp;
        }
        /*=============================================================================*/
        /**
         * No se logro ninguna jugada anterior por lo cual
         * podemos utilizar la suma de los dados de alguna
         * de las caras para verificar si podemos obtener
         * un punteo.
         */
        if (this.mJugador.getPunteo(5) == -1) {
            /**
             * Verificamos que la posicion
             * de la suma de 6 este libre
             * en el tablero
             */
            var temp = Number(rep[5]) * 6;
            posibilidades[5] = temp;
        }
        if (this.mJugador.getPunteo(4) == -1) {
            /**
             * Verificamos que la posicion
             * de la suma de 5 este libre
             * en el tablero
             */
            var temp = Number(rep[4]) * 5;
            posibilidades[4] = temp;
        }
        if (this.mJugador.getPunteo(3) == -1) {
            /**
             * Verificamos que la posicion
             * de la suma de 4 este libre
             * en el tablero
             */
            var temp = Number(rep[3]) * 4;
            posibilidades[3] = temp;
        }
        if (this.mJugador.getPunteo(2) == -1) {
            /**
             * Verificamos que la posicion
             * de la suma de 3 este libre
             * en el tablero
             */
            var temp = Number(rep[2]) * 3;
            posibilidades[2] = temp;
        }
        if (this.mJugador.getPunteo(1) == -1) {
            /**
             * Verificamos que la posicion
             * de la suma de 2 este libre
             * en el tablero
             */
            var temp = Number(rep[1]) * 2;
            posibilidades[1] = temp;
        }
        if (this.mJugador.getPunteo(0) == -1) {
            /**
             * Verificamos que la posicion
             * de la suma de 1 este libre
             * en el tablero
             */
            var temp = Number(rep[0]) * 1;
            posibilidades[0] = temp;
        }
        /*=============================================================================*/
        /**
         * Si no tenemos otra opcion entonces
         * podemos marcar el chance como ultima
         * alternativa de juego
         */
        if (this.mJugador.getPunteo(13) == -1) {
            /**
             * No entro a las opciones
             * anteriores de juego, por
             * lo cual marcamos chance
             */
            var temp = 0;
            for (var i = 0; i < rep.length; i++) {
                temp = temp + Number(rep[i]) * (i + 1);
            }
            posibilidades[13] = temp;
        }
        //Terminamos el total de tiros
        return posibilidades;
    }
    tomarTurno(dados) {
        //Varificamos que no termino
        //su tablero de juego
        if (this.juegoTerminado()) {
            return [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
        }
        return this.decision(dados);
    }
    jugadaElegida(pos, pts) {
        if (pos != -1 && pts != -1) {
            this.mJugador.setPunteo(pos, pts);
        }
        return "Se eligio [" + pos + ", " + pts + "]";
    }
    getNombre() {
        return this.nJugador;
    }
    getTablero() {
        return this.mJugador.getMatriz();
    }
    juegoTerminado() {
        return this.mJugador.llena();
    }
    puntajeFinal() {
        return this.mJugador.punteoFinal();
    }
    getScore() {
        return this.mJugador.score();
    }
}
exports.Juego = Juego;
