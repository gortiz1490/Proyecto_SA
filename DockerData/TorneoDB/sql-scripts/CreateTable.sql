
CREATE TABLE juego(
  id_juego INTEGER NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(50) NOT NULL,
  link VARCHAR(500) NOT NULL,

  PRIMARY KEY (id_juego)
);

CREATE TABLE torneo(
  id_torneo INTEGER NOT NULL AUTO_INCREMENT,
  id_juego INTEGER NOT NULL REFERENCES juego(id_juego),
  url_juego VARCHAR(500) NOT NULL,
  n_partidas INTEGER NOT NULL,

  PRIMARY KEY (id_torneo)
);

CREATE TABLE partido(
  id_partido VARCHAR(100) NOT NULL,
  id_torneo INTEGER NOT NULL REFERENCES torneo(id_torneo),

  PRIMARY KEY (id_partido)
);

CREATE TABLE llave(
  id_partido VARCHAR(100) NOT NULL REFERENCES partido(id_partido),
  id_jugador1 INTEGER NOT NULL DEFAULT 0,
  id_jugador2 INTEGER NOT NULL DEFAULT 0,
  score_jugador1 INTEGER NOT NULL DEFAULT 0,
  score_jugador2 INTEGER NOT NULL DEFAULT 0,

  PRIMARY KEY (id_partido, id_jugador1, id_jugador2)
);

CREATE TABLE flujoPartida(
  idAct VARCHAR(100) NOT NULL REFERENCES partido(id_partido),
  idPre1 VARCHAR(100) NOT NULL REFERENCES partido(id_partido),
  idPre2 VARCHAR(100) NOT NULL REFERENCES partido(id_partido),

  PRIMARY KEY (idAct, idPre1, idPre2)
);